#include "DHT.h"
#define DHTPIN 26  
#define DHTTYPE DHT11 
#define timeSeconds 1

// Set GPIOs for LED and PIR Motion Sensor

const int led = 19;
const int motionSensor = 14;
String movimiento;



float temperature = 0.0f;
float humidity = 0.0f;
String si = "SI";
String no = "NO";
DHT dht(DHTPIN, DHTTYPE);





// Timer: Auxiliary variables
unsigned long now = millis();
unsigned long lastTrigger = 0;
boolean startTimer = false;


// Checks if motion was detected, sets LED HIGH and starts a timer
void IRAM_ATTR detectsMovement() {
  Serial.println("Movimiento detectado!");

  digitalWrite(led, HIGH);
  startTimer = true;
  lastTrigger = millis();
}


void setup() 
{
  Serial.begin(9600);
  Serial1.begin(115200);
  dht.begin();  

  // PIR Motion Sensor mode INPUT_PULLUP
  pinMode(motionSensor, INPUT_PULLUP);
  // Set motionSensor pin as interrupt, assign interrupt function and set RISING mode
  attachInterrupt(digitalPinToInterrupt(motionSensor), detectsMovement, RISING);

  // Set LED to LOW
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);


  
}

void loop() 
{
  // read from port 1, send to port 0:
  if (Serial1.available()) {
    int inByte = Serial1.read();
    Serial.write(inByte);
    movimientoo();
     delay(2000);
    
   
  }

  // read from port 0, send to port 1:
  if (Serial.available()) {
    int inByte = Serial.read();
    Serial1.write(inByte);
    readSensor();
    ifmovimiento();
    sendTermicoToNextion();
    sendHumoToNextion();
    sendHumidityToNextion();
    sendTemperatureToNextion();
    sendMovimientoToNextion();
    delay(2000);
  }

}

void readSensor()
{
 humidity = dht.readHumidity();
 temperature = dht.readTemperature();

}

void movimientoo()
{
   // Current time
  now = millis();
  // Turn off the LED after the number of seconds defined in the timeSeconds variable
  if(startTimer && (now - lastTrigger > (timeSeconds*1000))) {
    Serial.println("Sin movimiento!");

    digitalWrite(led, LOW);
    startTimer = false;
  }
}

void ifmovimiento()
{
  if (led == LOW)
  {
    movimiento = no;
  }
  else if (led == HIGH)
  {
    movimiento = si;
  }
}

void sendHumidityToNextion()
{
  String command = "humidity.txt=\""+String(humidity)+"\"";
  Serial.print(command);
  endNextionCommand();
}

void sendTemperatureToNextion()
{
  String command = "temperature.txt=\""+String(temperature,1)+"\"";
  Serial.print(command);
  endNextionCommand();
}

void sendMovimientoToNextion()
{
  String command = "movimientop.txt=\""+String(no)+"\"";
  Serial.print(command);
  endNextionCommand();
}

void sendTermicoToNextion()
{
  String command = "termico.txt=\""+String(no)+"\"";
  Serial.print(command);
  endNextionCommand();
}

void sendHumoToNextion()
{
  String command = "humo.txt=\""+String(no)+"\"";
  Serial.print(command);
  endNextionCommand();
}


void endNextionCommand()
{
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
}
