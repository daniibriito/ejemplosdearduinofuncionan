#include <WiFiManager.h>
#include <WiFi.h>
#include <HTTPClient.h>

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEBeacon.h>
#include <BLEAdvertisedDevice.h>

#include <sstream>

//for LED status
#include <Ticker.h>

int scanTime = 15; //Delay de scaneo
BLEScan* pBLEScan;

Ticker ticker;

//Your Domain name with URL path or IP address with path
String serverName = "http://tornilleria-sanjose.com.ve/api/beacon";

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastTime = 0;

// Delay consulta
unsigned long timerDelay = 15000;

std::stringstream ss;

#ifndef LED_BUILTIN
#define LED_BUILTIN 2 // ESP32 DOES NOT DEFINE LED_BUILTIN
#endif

int LED = LED_BUILTIN;

void tick(){
  //toggle state
  digitalWrite(LED, !digitalRead(LED)); // Coloco el pin en el estado contrario.
}

//gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      if (advertisedDevice.haveManufacturerData()){

        char* manufacturerdata = BLEUtils::buildHexData(NULL, (uint8_t*)advertisedDevice.getManufacturerData().data(), advertisedDevice.getManufacturerData().length());

        char* str1 = BLEUtils::buildHexData(NULL, (uint8_t*)advertisedDevice.getManufacturerData().substr(0,2).data(), advertisedDevice.getManufacturerData().substr(0,2).length());
        char* str2 = "4c00";
        
        int result = strcmp(str1, str2);
        int allData = advertisedDevice.getManufacturerData().length();
        
        if (result==0 && allData ==25) {     
          Serial.printf("Advertised Device: %s \n", advertisedDevice.toString().c_str());
        }
 
      }
    }
};

void setup() {
  WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP
  // put your setup code here, to run once:
  Serial.begin(115200);
  
  //set led pin as output
  pinMode(LED, OUTPUT);
  // start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wm;
  //reset settings - for testing
  // wm.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wm.setAPCallback(configModeCallback);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wm.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.restart();
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");
  ticker.detach();
  //keep LED on
  digitalWrite(LED, LOW);

  Serial.println("Scanning...");
  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan(); //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);  // less or equal setInterval value
  
}

void loop() {
  // put your main code here, to run repeatedly:
  BLEScanResults foundDevices = pBLEScan->start(scanTime, false);

  int count = foundDevices.getCount();

  int fistTime = 0;
  
  ss.str("");
  
  ss << "[";
  for (int i = 0; i < count; i++) {
  
    BLEAdvertisedDevice d = foundDevices.getDevice(i);

    if (d.haveManufacturerData()) {

      char* manufacturerdata = BLEUtils::buildHexData(NULL, (uint8_t*)d.getManufacturerData().data(), d.getManufacturerData().length());
  
      char* str1 = BLEUtils::buildHexData(NULL, (uint8_t*)d.getManufacturerData().substr(0,2).data(), d.getManufacturerData().substr(0,2).length());
      char* str2 = "4c00";
      
      int result = strcmp(str1, str2);
      int allData = d.getManufacturerData().length();
      
      if (result==0 && allData ==25) {

        if (fistTime > 0) {
          ss << ",";
        }
       
        ss << "{\"Address\":\"" << d.getAddress().toString() << "\",\"Rssi\":" << d.getRSSI();

        if (d.haveName()) {
          ss << ",\"Name\":\"" << d.getName() << "\"";
        }

        if (d.haveAppearance()) {
          ss << ",\"Appearance\":" << d.getAppearance();
        }

        if (d.haveManufacturerData()) {
          std::string md = d.getManufacturerData();
          uint8_t *mdp = (uint8_t *)d.getManufacturerData().data();
          char *pHex = BLEUtils::buildHexData(nullptr, mdp, md.length());
          ss << ",\"ManufacturerData\":\"" << pHex << "\"";
          free(pHex);
        }

        if (d.haveServiceUUID()) {
          ss << ",\"ServiceUUID\":\"" << d.getServiceUUID().toString() << "\"";
        }

        if (d.haveTXPower()) {
          ss << ",\"TxPower\":" << (int)d.getTXPower();
        }

        ss << "}";

        fistTime = fistTime + 1;
      }
    }
  }
  ss << "]";
    
  Serial.println("Scan done!");
  pBLEScan->clearResults();   // delete results fromBLEScan buffer to release memory

   //Send an HTTP POST request every 10 minutes
  if ((millis() - lastTime) > timerDelay) {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED) {
      HTTPClient http;
      
      // Your Domain name with URL path or IP address with path
      http.begin(serverName);

      Serial.printf("Data To Send: %s \n", ss.str().c_str());
      
      
      // If you need an HTTP request with a content type: application/json, use the following:
      http.addHeader("Content-Type", "application/json");
      int httpResponseCode = http.POST(ss.str().c_str());


      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
        
      // Free resources
      http.end();
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
  
  delay(2000);
}
