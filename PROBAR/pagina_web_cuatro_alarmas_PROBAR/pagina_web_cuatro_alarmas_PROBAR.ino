                            // Incluyendo librerías.
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiAP.h>

#define LED_BUILTIN 2       // Configure el pin GPIO donde conectó su LED de prueba o comente esta línea si su placa de desarrollo tiene un LED incorporado

                            // Credenciales de WiFi.
const char *ssid = "Daniela Brito";
const char *password = "27349996";

                            // Servidor Web en puerto 80.
WiFiServer server(80);

                              // Para utilizarlo con una IP fija.
IPAddress IP(192,168,1,2);
IPAddress subnet(255,255,255,0); 

                             // VARIABLES GLOBALES.
int contconexion = 0;

String header;              // Variable para guardar el HTTP request

String estadoSalida = "off";
const int salida = 2;

                            // CÓDIGO HTML
String pagina = "<!DOCTYPE html>"
"<html>"
"<head>"
"<meta charset='utf-8' />"
"<title>Servidor Web ESP32</title>"
"</head>"
"<body>"
"<center>"
"<h1>Servidor Web ESP32</h1>"
"<p><a href='/on'><button style='height:50px;width:100px;background-color:#CD5C5C'>1 ALARMA</button></a></p>"
"<p><a href='/off'><button style='height:50px;width:100px;background-color:#F08080'>2 ALARMA</button></a></p>"
"<p><a href='/on'><button style='height:50px;width:100px;background-color:#FFA07A'>3 ALARMA</button></a></p>"
"<p><a href='/off'><button style='height:50px;width:100px;background-color:#FFCABE '>4 ALARMA</button></a></p>"
"</center>"
"</body>"
"</html>";


                             // Configuración.
void setup() {
  pinMode(salida, OUTPUT);

  Serial.begin(115200);
  Serial.println();
  Serial.println("Configuring access point...");

  // You can remove the password parameter if you want the AP to be open.
  WiFi.softAPConfig(IP, IP, subnet);

  // Conexión WIFI
  WiFi.softAP(ssid, password);
  
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.begin(); // se inicia el servidor

  Serial.println("Server started");
}

                                            //  LOOP 

void loop() {
  WiFiClient client = server.available();   // Escucha a los clientes entrantes.

  if (client) {                             // Si se conecta un nuevo cliente, entonces:
    Serial.println("New Client.");          // Imprime un mensaje desde el monitor serie.
    String currentLine = "";                // Hacer una cadena para contener los datos entrantes del cliente.
    while (client.connected()) {            // Loop mientras el cliente está conectado.
      if (client.available()) {             // Si hay bytes para leer desde el cliente, entonces:
        char c = client.read();             // Lee un byte.
        Serial.write(c);                    // Imprime ese byte en el monitor serial.
        header += c;
        if (c == '\n') {                    // Si el byte es un caracter de salto de linea.

          // Si la línea actual está en blanco, tiene dos caracteres de nueva línea seguidos.
          // Final de la solicitud HTTP del cliente, así que envíe una respuesta:
          if (currentLine.length() == 0) {
             // si la nueva linea está en blanco significa que es el fin del 
             // HTTP request del cliente, entonces se responde:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();

              // enciende y apaga el GPIO
            if (header.indexOf("GET /on") >= 0) {
              Serial.println("GPIO on");
              estadoSalida = "on";
              digitalWrite(salida, HIGH);    //Prende el led
            } else if (header.indexOf("GET /off") >= 0) {
              Serial.println("GPIO off");
              estadoSalida = "off";
              digitalWrite(salida, LOW);    //Apaga el led
            }
            
                                            // Muestra la página web
            client.println(pagina);
            
                                            // La respuesta HTTP termina con una línea en blanco.
            client.println();
            break;
          } else {                          // Si se tiene una nueva línea, se limpiacurrentLine.
            currentLine = "";
          }
        } else if (c != '\r') {             // Si C es distinto al caracter de retorno de carro.
          currentLine += c;                 // Lo agrega al final de currentLine.
        }
      }
    }
                                            // Se limpia la variable header.
    header = "";
 
                                            // Se cierra la conexión.
    client.stop();
    Serial.println("Cliente desconectado.");
    Serial.println("");
  }
}
