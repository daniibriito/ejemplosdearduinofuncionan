#include <ArduinoJson.h>
 
void SerializeObject()
{
    String json;
    StaticJsonDocument<300> doc;
    doc["name"] = "alarm02";
    doc["id"] = 1;
    doc["id_user"] = 1;
    doc["location"] = "cabudare";
    doc["active"] = 1;
    doc["humo"] = true;
    doc["movimiento"] = true;
    doc["calor"] = true;
    doc["temperatura"] = 31.2;
    doc["humedad"] = 1.14;
 
    serializeJson(doc, json);
    Serial.println(json);
}
 
void DeserializeObject()
{
    String json = "{\"name\":\"alarm01\",\"id\":1,\"id_user\":1,\"location\":\"cabudare\",\"active\":1,\"humo\":true,\"movimiento\":true,\"calor\":true,\"temperatura\":\"31.2\",\"humedad\":1.14}";
 
    StaticJsonDocument<300> doc;
    DeserializationError error = deserializeJson(doc, json);
    if (error) { return; }

    String name = doc["name"];
    int id = doc["id"];
    int id_user = doc["id_user"];
    String location = doc["location"];
    int active = doc["active"];
    bool humo = doc["humo"];
    bool calor = doc["calor"];
    bool movimiento = doc["movimiento"];
    float temperatura = doc["temperatura"];
    float humedad = doc["humedad"];
 
    Serial.println(name);
    Serial.println(id);
    Serial.println(id_user);
    Serial.println(location);
    Serial.println(active);
    Serial.println(humo);
    Serial.println(calor);
    Serial.println(movimiento);
    Serial.println(temperatura);
    Serial.println(humedad);
}
 
void setup()
{
    Serial.begin(115200);
 
    Serial.println("===== Object Example =====");
    Serial.println("-- Serialize --");
    SerializeObject();
    Serial.println();
    Serial.println("-- Deserialize --");
    DeserializeObject();
 
}
 
void loop()
{
}
