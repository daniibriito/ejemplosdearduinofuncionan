
#include <WiFi.h>

const char* ssid = "TicktapsEAP";                        //SSID de nuestro router.
const char* password = "software";                //Contraseña de nuestro router.


void setup() {
   Serial.begin(115200);
   WiFi.begin(ssid,password); 
   Serial.println("Connecting to Wifi");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("\nConnected to the Wifi network");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  if ((WiFi.status() == WL_CONNECTED))
  {
    Serial.println("You can try to ping me");
    delay (5000);
    }
    else
    {
      Serial.println("Connection lost");
      }

}
