
void setup() {
   pinMode(2, OUTPUT);                 /* Se configura el pin 2 como pin de salida, siendo este el led interno del ESP32. */
   Serial.begin(9600);                 /* Se Inicia el monitor serie a 9600 baudios. */
}

void loop() {
   digitalWrite(2, HIGH);              /* Enciende el LED en alto. */
   Serial.println("LED ESTADO: ALTO"); /* Se muestra por el monitor serie "LED ESTADO: ALTO". */
   delay(1000);                        /* Espera 1 seg. */
   digitalWrite(2, LOW);               /* Se apaga el LED en bajo. */
   Serial.println("LED ESTADO: BAJO"); /* Se muestra por el monitor serie "LED ESTADO: BAJO". */
   delay(1000);                        /* Espera 1 seg. */
}
