String option;                    // Variable donde se guardará lo que se escriba en el monitor serie.
int led = 2;                      // Pin en donde se encuentra conectado el led interno del ESP32.
 
void setup(){
  Serial.begin(9600);            // Se Inicia el monitor serie a 9600 baudios. 
  pinMode(led, OUTPUT);          // Inicializa el pin LED como salida:
}
 
void loop(){
  
  //Si existe datos disponibles los lee:
  
  if (Serial.available()>0){
    
    
    option=Serial.readStringUntil('\n'); //Se lee la opción enviada (lee toda la oración por lo que se puede escribir letras.)
    if(option== "off") {                //Sí se escribe off en el monitor serie, el led se apagará.
      digitalWrite(led, LOW);           //Apaga el led.
      Serial.println("OFF");            //Escribir OFF en el monitor
    }
     else if(option== "OFF") {          //Sí se escribe OFF en el monitor serie, el led se apagará.
      digitalWrite(led, LOW);           //Apaga el led.
      Serial.println("OFF");            //Escribir OFF en el monitor
    }
     else if(option== "on") {           //Sí se escribe on en el monitor serie, el led se encenderá.
      digitalWrite(led, HIGH);          //Enciende el led.
      Serial.println("ON");             //Escribir ON en el monitor
    }
     else if(option== "ON") {           //Sí se escribe on en el monitor serie, el led se encenderá.
      digitalWrite(led, HIGH);          //Enciende el led.
      Serial.println("ON");             //Escribir ON en el monitor
    }
    else {
      Serial.println("INVÁLIDO");       // Escribir inválido en el monitor y mantiene el led como se encuentre en ese momento.
      }
  }
}
