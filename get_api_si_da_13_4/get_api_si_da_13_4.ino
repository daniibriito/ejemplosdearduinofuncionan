/*
  Rui Santos
  Complete project details at Complete project details at https://RandomNerdTutorials.com/esp32-http-get-post-arduino/

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

#include <WiFi.h>
#include <HTTPClient.h>

const char* ssid = "TicktapsEAP";
const char* password = "software";

int sensorhumoValue=1;
int sensorcalorValue=1;
int sensormovimientoValue=1;
int numid=20;
int sensortemperaturaValue=31;
int sensorhumedadValue=5;

String nameid = "&id_alarma=";
String namehumo = "&humo=";
String namecalor = "&calor=";
String nametemperatura = "&temperatura=";
String namehumedad = "&humedad=";
String namemovimiento = "&movimiento=";
String namedate = "&date=2020-04-14";



boolean control = true;




//Your Domain name with URL path or IP address with path
String serverName =  "https://apidimraw.myticktap.com/api/1.0/alarma/monitor/guardar?";

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastTime = 0;
// Timer set to 10 minutes (600000)
//unsigned long timerDelay = 600000;
// Set timer to 5 seconds (5000)
unsigned long timerDelay = 5000;

void setup() {
  Serial.begin(115200); 

  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
 
  Serial.println("Timer set to 5 seconds (timerDelay variable), it will take 5 seconds before publishing the first reading.");

 
         
}

void loop() {
  //Send an HTTP POST request every 10 minutes
  if ((millis() - lastTime) > timerDelay) {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
      HTTPClient http;
      String serverPath;
       if (control == true)
       {
        nameid.concat(numid);
        namehumo.concat(sensorhumoValue);
        namecalor.concat(sensorcalorValue);
        nametemperatura.concat(sensortemperaturaValue);
        namehumedad.concat(sensorhumedadValue);
        namemovimiento.concat(sensormovimientoValue);
          Serial.println(nameid);
          Serial.println(namehumo);
          Serial.println(namecalor);
          Serial.println(nametemperatura);
          Serial.println(namehumedad);
          Serial.println(namemovimiento);
          
          control=false;
         
        } 
        delay (5000);
     serverPath = serverName + nameid + namehumo + namecalor + nametemperatura + namehumedad + namemovimiento + namedate;   
     


      // Your Domain name with URL path or IP address with path
      http.begin(serverPath.c_str());
      
      // Send HTTP GET request
      int httpResponseCode = http.GET();
      
      if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        String payload = http.getString();
        Serial.println(payload);
      }
      else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
      }
      // Free resources
      http.end();
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
 
  
}
