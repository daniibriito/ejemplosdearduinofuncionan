#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>



const char* ssid = "TicktapsEAP";                        //SSID de nuestro router.
const char* password = "software";                //Contraseña de nuestro router.


void setup() {
   Serial.begin(115200);
   WiFi.begin(ssid,password); 
   Serial.println("Connecting to Wifi");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  
  Serial.println("\nConnected to the Wifi network");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  if ((WiFi.status() == WL_CONNECTED))
  {
    long rnd = random(1, 10);
    HTTPClient client;

    client.begin("https://apidimraw.myticktap.com/api/1.0/alarma/monitor/guardar?id_alarma=1&humo=20&calor=10&temperatura=10&humedad=10&movimiento=20?" + String(rnd));
    int httpCode= client.GET();

    if (httpCode > 0){
    String payload = client.getString();
    Serial.println("\nStatuscode: " + String(httpCode));
    Serial.println(payload);

    }
    else{
      Serial.println("Error on HTTP request");
    }
    
    }
    else
    {
      Serial.println("Connection lost");
      }
      delay (10000);

}
