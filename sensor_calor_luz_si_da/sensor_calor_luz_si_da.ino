

// Se establecen las constantes de los pines.
const int sensor_calor = 33;     //entrada DONDE SE CONECTA EL BOTÓN. (boot)

//salidas
const int ledPin =  22;       //DONDE SE CONECTA EL LED. (Led interno del esp32)
const int luz =  15; 
const int power_sensor =  4;       //DONDE SE CONECTA EL LED. (Led interno del esp32)


int buttonState = 0;         //VARIABLE DE ALMACENAMIENTO.

void setup() {
                                
  pinMode(ledPin, OUTPUT);   // Inicializa el pin LED como salida:
   pinMode(luz, OUTPUT);
  pinMode(power_sensor, OUTPUT);   // Inicializa el pin LED como salida:
  
   digitalWrite(ledPin, LOW);
    digitalWrite(luz, LOW);
    digitalWrite(power_sensor, LOW);
    
  pinMode(sensor_calor, INPUT); // Inicializa el botón como entrada:
    Serial.begin(9600);                 /* Se Inicia el monitor serie a 9600 baudios. */
}

void loop() {
  // Lee el estado del valor del botón pulsador, lo que se va a almacenar en esa variable:
  buttonState = digitalRead(sensor_calor); 
  digitalWrite(power_sensor, HIGH);
  delay (300);

  // Comprueba si el pulsador está presionado. Si es así, el buttonState es ALTO:
  if (buttonState == HIGH) {      //SI LO ALMACENADO ES = ALTO O QUE EL BOTON ESTA PRESIONADO ENTONCES EL LED DEBE DE PRENDER
    
    digitalWrite(ledPin, LOW);
    digitalWrite(luz, LOW);
    Serial.println("SENSOR ESTADO: DESACTIVADO"); /* Se muestra por el monitor serie "SENSOR ESTADO: ALTO". */
    
  } else {                        // SI NO 
    
    digitalWrite(ledPin, HIGH);    //SI EL BOTON NO ESTÁ PULSIONADO ENTONCES EL LED SE APAGARÁ.
    digitalWrite(luz, HIGH);  
    digitalWrite(power_sensor, LOW);
    delay (5000); 
    digitalWrite(power_sensor, HIGH);
     Serial.println("SENSOR ESTADO: ACTIVADO"); /* Se muestra por el monitor serie "SENSOR ESTADO: BAJO". */
 
  }
}
