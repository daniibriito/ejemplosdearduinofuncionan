          
#include <EEPROM.h>                   //Incluir biblioteca para leer y escribir desde la memoria EEPROM.
 
#define EEPROM_SIZE 1                //Define el número de bytes a los que se quiere acceder.

                                     // Las constantes no cambian. Se utilizan aquí para establecer números de pines:
const int buttonPin = 0;             // Pin donde se encuentra conectado el botón (boot).
const int ledPin = 2;                // Pin donde se encuentra conectado el led interno del ESP32.

                                     // Variables declaraciones:
int ledState = HIGH;                 // El estado del pin de salida será alto.
int buttonState;                     // Lectura actual del botón.
int lastButtonState = LOW;           // La lectura anterior del botón.


unsigned long lastDebounceTime = 0;  // La última vez que se alteró el pin de salida.
unsigned long debounceDelay = 50;    // El tiempo antirrebote; aumenta sí la salida parpadea.

void setup() { 
  Serial.begin(115200);
  EEPROM.begin(EEPROM_SIZE);        // Inicializar EEPROM con un tamaño predefinido

  pinMode(buttonPin, INPUT);        //Declara el botón como entrada.
  pinMode(ledPin, OUTPUT);          //Declara el led como salida.

 ledState = EEPROM.read(0);         // Lee el último estado del LED de la memoria flash

  
  digitalWrite(ledPin, ledState);   // Establece el LED en el último estado almacenado
}

void loop() {
    
  int reading = digitalRead(buttonPin); //Leer el estado del conmutador en una variable local:

  
  if (reading != lastButtonState) {    // Si el interruptor cambió, debido a ruido o presionando:
    
    lastDebounceTime = millis();      // Reestablecer el temporizador de la eliminación de los rebotes.
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
                                       // Cualquiera que sea la lectura, ha estado por más tiempo que el rebote
                                       // de retraso, por lo que se toma como el estado actual real:

                                       // Sí el estado del botón ha cambiado:
    if (reading != buttonState) {
      buttonState = reading;

                                       //  Sólo cambie el LED si el nuevo estado del botón es ALTO.
      if (buttonState == HIGH) {
        ledState = !ledState;
      }
    }
  }
                                      // Guarda la lectura. La próxima vez a través del bucle, será el lastButtonState:
  lastButtonState = reading;
  
                                      // Si la variable ledState es diferente del estado actual del LED.
  if (digitalRead(ledPin)!= ledState) {  
    Serial.println("Estado cambiado");
                                      // Cambiar el estado del LED.
    digitalWrite(ledPin, ledState);
                                     // Guardar el estado del LED en la memoria flash.
    EEPROM.write(0, ledState);
    EEPROM.commit();
    Serial.println("Estado guardado en memoria flash");
  }
}
